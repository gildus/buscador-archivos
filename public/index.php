<?php

define ('PATH_APP_BUSCADOR_ARCHIVOS', dirname(__DIR__) . '/');

require __DIR__ . './../vendor/autoload.php';

(new App\Application())->run();