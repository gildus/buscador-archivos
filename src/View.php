<?php

namespace App;

class View
{
    public function __construct($params = [])
    {
        if ($params && count($params) > 0) {
            foreach ($params as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    public function render($file)
    {
        if (file_exists($file)) {
            ob_start();
            include $file;
            $content = ob_get_clean();
            echo $content;
            exit(0);
        }

        echo 'View not found';
        exit(1);
    }
}