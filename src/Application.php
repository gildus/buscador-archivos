<?php

namespace App;

use EndorphinStudio\Detector\Detector;


class Application extends Core
{
    private $agentDetect;

    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        if (!$this->hasConfig()) {
            return $this->settingConfig();
        }

        return $this->homeBusqueda();
    }

    private function hasConfig()
    {
        if (file_exists(PATH_APP_BUSCADOR_ARCHIVOS . 'path_files.txt') === true) {
            $content = trim(file_get_contents(PATH_APP_BUSCADOR_ARCHIVOS . 'path_files.txt'));
            return strlen($content) > 0;
        }

        return false;
    }

    private function settingConfig()
    {
        $result = null;
        if (isset($_POST['process']) && $_POST['process'] == 'config') {
            $directorio = trim($_POST['directorio'] ?: '');
            if (file_exists($directorio)) {
                if (file_put_contents(PATH_APP_BUSCADOR_ARCHIVOS . 'path_files.txt', $directorio) !== false) {
                    header("Location: /");
                    exit(0);
                } else {
                    $result = [
                        'error' => 'Ocurrio un error, al configurar el directorio donde se realizara las busquedas, por favor vuelva ha intentarlo.'
                    ];
                }
            } else {
                $result = [
                    'error' => 'Verifica que la ruta existe. Vuelve ha intentarlo por favor.'
                ];
            }
        }

        return $this->view($result);
    }

    private function homeBusqueda()
    {
        $resultSearch = [];
        if (isset($_POST['process']) && isset($_POST['q']) && $_POST['process'] == 'search' && !empty($_POST['q'])) {
            try {
                $query = filter_var($_POST['q'], FILTER_SANITIZE_STRING);

                $pathFiles = file_get_contents(PATH_APP_BUSCADOR_ARCHIVOS . 'path_files.txt');
                $pathFiles = trim($pathFiles);

                if ($pathFiles) {

                    $results = $this->locateFiles($pathFiles, $query);
                    $results = str_replace(array("\r\n", "\r", "\n"), "<br />", $results);
                    $resultSearch = [
                        'result' => $results ? array_filter(explode('<br />', $results)) : [],
                    ];
                }

            } catch (\Exception $ex) {

            }
        }

        return $this->view($resultSearch);
    }
}