<?php

namespace App;

class Core
{
    public function __construct()
    {
        if (isset($_SERVER['SCRIPT_NAME']) && $_SERVER['SCRIPT_NAME'] !== '/') {
            if (file_exists(PATH_APP_BUSCADOR_ARCHIVOS . 'public' . $_SERVER['SCRIPT_NAME'])) {

                $ext = pathinfo(PATH_APP_BUSCADOR_ARCHIVOS . 'public' . $_SERVER['SCRIPT_NAME'], PATHINFO_EXTENSION);
                if (in_array($ext, ['css', 'js'])) {

                    if ($ext == 'css') {
                        header("Content-type: text/css; charset: UTF-8");
                    } elseif($ext == 'js') {
                        header('Content-Type: application/javascript');
                    }

                    echo file_get_contents(PATH_APP_BUSCADOR_ARCHIVOS . 'public' . $_SERVER['SCRIPT_NAME']);
                    exit (0);
                }

            }

            echo 'File not Found';
            exit(1);
        }
    }

    protected function view($params = [])
    {
        $trace = debug_backtrace();
        $view = $trace[1]['function'] ?: null;
        $oView = new View($params);
        $oView->render(PATH_APP_BUSCADOR_ARCHIVOS . 'src/view/' . $view . '.phtml');
    }

    protected function locateFiles($pathFiles, $query)
    {
        $pathFiles = realpath($pathFiles);
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $command = escapeshellcmd("FINDSTR /s /m /I ".$query." ".$pathFiles."\*.txt");
        } else {
            $command = escapeshellcmd("grep -rl '".$pathFiles."' -e '".$query."'");
        }

        ob_start();
        passthru($command);
        $results = ob_get_contents();
        ob_end_clean();

        return $results;
    }
}