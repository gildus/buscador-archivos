<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3fdee1fa2b416fb1163ea35cee6472be
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3fdee1fa2b416fb1163ea35cee6472be::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3fdee1fa2b416fb1163ea35cee6472be::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
